# Refill SDK

## Implementation
Make sure root `build.gradle` includes refillsdk's source repository
``` gradle
allprojects {
    repositories {
        maven {
            url 'https://gitlab.com/api/v4/projects/26819858/packages/maven'
        }
    }
}
```

And refillsdk dependency is added to app `build.gradle`
``` gradle
dependencies {
    implementation 'se.smartrefill.android:refillsdk:2.1.0'
}
```

## Initialization
``` kotlin
// configuration example
val config = SDKConfig(
    apiKey = getString(R.string.sdk_api_key),
    applicationId = BuildConfig.APPLICATION_ID,
    applicationVersionName = BuildConfig.VERSION_NAME,
    applicationVersionCode = BuildConfig.VERSION_CODE,
    company = getString(R.string.refill_sdk_company),
    environment = if (BuildConfig.DEBUG) EnvironmentConfig.TEST else EnvironmentConfig.PRODUCTION,
    language = LanguageConfig.getFromCurrentDefaultLocale(),
    countryCode = Locale.getDefault().country,
    urlCallbackConfig = UrlCallbackConfig(
        urlLaunchMethod = UrlLaunchModeMethod.EMBEDDED,
        // see section about <intent-filter> node in AndroidManifest.xml for intentFilterScheme
        paymentPortalIntentFilterScheme = getString(R.string.default_intent_filter_scheme_refill)
    ),
    enableLog = BuildConfig.DEBUG
)

// initialize platform
RefillSdk.init(this, config)
```

## Usage
All parts of the platform can now be accessed using `RefillSdk` singleton instance
``` kotlin
RefillSdk.getInstance()
```

Optionally, since the `RefillSdk` instance exists as a singleton, we might want to use convenient
extension functions to omit typing `RefillSdk.getInstance()` every time within `Activity` or `Fragment` context.
Take this extensive list of possible use cases as an example, covering most of the functionality:
``` kotlin
// shortcuts to sdk
fun Any.sdk(): RefillSdk = RefillSdk.getInstance()

// shortcuts to authentication module
fun Any.sdkAuthentication(): AuthenticationModule = sdk().authenticationModule
fun Any.sdkAuthenticationService(): AuthenticationService = sdkAuthentication().service
fun Any.sdkAuthenticationRepository(): AuthenticationRepository = sdkAuthentication().repository

// shortcuts to content module
fun Any.sdkContent(): ContentModule = sdk().contentModule
fun Any.sdkContentService(): ContentService = sdkContent().service
fun Any.sdkContentRepository(): ContentRepository = sdkContent().repository

// shortcuts to product module and some of its methods
fun Any.sdkProduct(): ProductModule = sdk().productModule
fun Any.sdkProductService(): ProductService = sdkProduct().service
fun Any.sdkProductRepository(): ProductRepository = sdkProduct().repository
fun ProductModule.getAllProducts(): List<Product> = sdkProductRepository().catalogs.getAllProducts()
fun ProductModule.getProductById(id: String): Product? = sdkProductRepository().catalogs.getProductById(id)
fun ProductModule.getProductByReference(reference: String): Product? = sdkProductRepository().catalogs.getProductByReference(reference)

// shortcuts to refill module
fun Any.sdkRefill(): RefillModule = sdk().refillModule
fun Any.sdkAuthentication(): AuthenticationService = sdkRefill().authenticationService
fun Any.sdkCustomer(): CustomerService = sdkRefill().customerService
fun Any.sdkPhone(): PhoneService = sdkRefill().phoneService
fun Any.sdkCardRegistration(): CardRegistrationService = sdkRefill().cardRegistrationService
fun Any.sdkOrder(): OrderService = sdkRefill().orderService
fun Any.sdkSubscription(): SubscriptionService = sdkRefill().subscriptionService
fun Any.sdkAgreement(): AgreementService = sdkRefill().agreementService
fun Any.sdkReceipt(): ReceiptService = sdkRefill().receiptService
fun Any.sdkRewards(): RewardsService = sdkRefill().rewardsService
fun Any.sdkRefillRepository(): RefillRepository = sdk().refillModule.repository
fun RefillRepository.isLoggedInLocally() = customer?.isLoggedInLocally() ?: false

// shortcuts to sim registration module
fun Any.sdkSimRegistration(): SimRegistrationModule = sdk().simRegistrationModule
fun Any.sdkSimRegistrationService(): SimRegistrationService = sdkSimRegistration().service
fun Any.sdkSimRegistrationRepository(): SimRegistrationService = sdkSimRegistration().service
```

When any suspended function from any of the services is called, it usually means that SDK will communicate with the backend.
Result of that communication will be wrapped in a returned `Result` .
RefillSdk offers convenience functions to invoke suspended calls toward the backend with `initCall()` and `initCalls()` .
``` kotlin
sdk().initCall({ sdkProductService().getCatalogs() }) {
   // result containing catalogs retrieved, or an error with info
}

sdk().initCalls(
    predicates = arrayOf(
        { sdkContentService().getAll() },
        { sdkProductService().getCatalogs() }
    )
    resultListener = {
        // accumulated result from both predicates
    },
)
```

`sdk().initCall` and `sdk().initCalls` are just convenience methods and you're not required to use it.
Another way would be to use coroutines to handle calls to suspended functions
``` kotlin
lifecycleScope.launchWhenStarted {
    sdkCustomer().service.getCustomer(customerId)
        .onSuccess {
            // something to do on success (eg refresh ui)
        }
        .onError { errorMessage, developerMessage, errorCode, errorType ->
            // something to do on error (eg show toast)
        }
    }
}    
```

You can also set up an extensions to `CoroutineScope`, so you can call `initCall` or `initCalls` from it directly.
``` kotlin
fun <T : Any> CoroutineScope.initCall(
    predicate: suspend () -> Result<T>,
    invokeResultOnMainThread: Boolean = true,
    resultListener: ((Result<T>) -> Unit)? = null
) {
    sdk().initCall(
        predicate = predicate,
        coroutineScope = this,
        invokeResultOnMainThread = invokeResultOnMainThread,
        resultListener = resultListener
    )
}

fun CoroutineScope.initCalls(
    vararg predicates: suspend () -> Result<Any>,
    invokeResultOnMainThread: Boolean = true,
    invokeCallsAsync: Boolean = true,
    resultsListener: ((results: List<Result<Any>>) -> Unit)? = null,
    resultListener: ((aggregatedResult: Result<Any>) -> Unit)? = null,
) {
    sdk().initCalls(
        predicates = predicates,
        coroutineScope = this,
        invokeCallsAsync = invokeCallsAsync,
        invokeResultOnMainThread = invokeResultOnMainThread,
        resultsListener = resultsListener,
        resultListener = resultListener
    )
}
```

**During most backend calls, data is cached in a local repository.** Each module is responsible of maintaining its
own repository implicitly, so the **cached data is always up to date**. Repository properties are always read-only.
Data can be safely be retrieved from the repository from any thread.
For example:
``` kotlin
// retrieve string from content repository by it's key
sdkContentRepository().getString("<string_key>")

// retrieve single product from repository by it's reference
sdkProductRepository().getAllCatalogs().getProductByReference("<product_reference>")

// retrieve current customer
sdkRefillRepository().customer

// retrieve sim registration status for a given number
sdkRefillRepository().getSimRegistrationStatus("<phoneNumber>")
```

Additionally, make sure to pass `Intent` from `Activity.onNewIntent` into the sdk and tell it when
app resumes on `Activity.onResume`, as services like `OrderService` or `SubscriptionService` require it,
since sdk doesn't own the application’s lifecycle.
``` kotlin
override fun onNewIntent(intent: Intent?) {
   super.onNewIntent(intent)
   // pass intent events into sdk in case it needs to handle any flows
   sdk().onNewIntent(intent)
}

override fun onResume() {
    super.onResume()
    // let sdk know when activity resumed so it knows better how to fine-tine any active flows
    sdk().onResumedAppLifecycle()
}
```

In order for `Activity.onNewIntent`, we have to make sure the `<intent-filter>` in `AndroidManifest.xml`
is configured properly for the activity initializing the payments.
``` xml
<activity
    android:name="..."
    android:launchMode="singleTask">
    <intent-filter>
        <action android:name="android.intent.action.VIEW" />
        <category android:name="android.intent.category.DEFAULT" />
        <category android:name="android.intent.category.BROWSABLE" />
        <!-- RefillConfig intentFilterScheme values have to match here if module used -->
        <data android:scheme="@string/default_intent_filter_scheme_refill" android:host="success" />
        <data android:scheme="@string/default_intent_filter_scheme_refill" android:host="failure" />
        <!-- SimRegistrationConfig intentFilterScheme values have to match here if module used -->
        <data android:scheme="@string/default_intent_filter_scheme_simreg" android:host="success" />
        <data android:scheme="@string/default_intent_filter_scheme_simreg" android:host="failure" />
        <!-- UrlCallbackConfig intentFilterScheme values have to match here if module used -->
        <data android:scheme="@string/default_intent_filter_scheme_urlcallback android:host="success" />
        <data android:scheme="@string/default_intent_filter_scheme_urlcallback" android:host="failure" />
    </intent-filter>
</activity>
```


SDK is also capable of sending global events originating from potentially any call that map to
specific global error.
To use it, implement GlobalErrorListener interface, add listener as needed and remove it when done with it.
``` kotlin
class MainActivity : AppCompatActivity(), GlobalErrorListener {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        // this activity will listen to global errors
        sdk().addGlobalErrorListener(this)
    }

    override fun onDestroy() {
        super.onDestroy()

        // cleanup
        sdk().cancelAllCalls()
        sdk().removeGlobalErrorListener(this)
    }

    override fun onAuthorizationError(errorMessage: String?) {
        // TODO handle authorization error
    }

    override fun onDownForMaintenanceError(errorMessage: String?) {
        // TODO handle down on maintainance error
    }

    override fun onEndOfLifeError(errorMessage: String?) {
        // TODO handle end of life error
    }
}
```


## Documentation
Project includes sources.jar and javadoc.jar


## Author
Marcel Vojtkovszky, <marcel.vojtkovszky@smartrefill.se>


## License
RefillSDK is propriety and can not be used with out explicit permission from Smart Refill AB.
Refer to LICENSE.md for more info.
