# CHANGELOG

## 2.1.0 (2025-02-13)
* Remove errorCode from `Result.Error`.
* Rename `WebPortalConfig` to `UrlCallbackConfig` and add `authOtpIntentFilterScheme` to it.
* Bump kotlinx-coroutines-android to 1.10.10, kotlinx-serialization-json to 1.8.0

## 2.0.0 (2024-12-20)
* Add support for email authentication. Added methods to `AuthenticationService` with support to login, register account and verify email using OTP.
* Add new `featureTogglesService` to `RefillModule`. 
* Make properties in `Bucket` optional.
* Add `status` to `OrderStatus`. Allows us to provide better error messages using `OrderStatus.getErrorMessageFromRefillStatus`.
* Remove `RefillSDK.setDefaultErrorMessages`. Default error messages are now handled internally and localized to all supported languages.
* Bump Kotlin to 2.0.21, Gradle plugin to 8.7.3
* Bump kotlinx-coroutines-android to 1.9.0
* Bump androidBuildToolsVersion to 35.0.0, androidCompileSdkVersion and androidTargetSdkVersion to 35

## 1.1.0 (2024-09-24)
* Add `SDKConfig` which changes how SDK is initialized with `RefillSDK.init(Context, SDKConfig)`.
* Make properties related to bonus points in `Customer` and `Phone` objects optional.
* Bump Kotlin to 2.0.20, Gradle plugin to 8.6.1
* Bump browser to 1.8.0, serialization-json to 1.7.3
* Bump retrofit to 2.11.0

## 1.0.6 (2024-02-16)
* Change `AuthenticationService.logout` result body returning `String` containing message instead of `Boolean`
* Change `CustomerService.deleteCustomer` result body returning `String` containing message instead of `Boolean`
* Add `SimStatus.isUnregistered` helper function to resolve the registration requirement status.
* Add `SimRegistrationRepository.getStatusesNeedRegistration` method to return unregistered statuses only.
* Add `SimRegistrationRepository.removeSimRegistrationStatus(phoneNumber)` method to allow removal of status from repository.
* Add `SimRegistrationRepository.clearStatuses` method to allow removal of all statuses in repository.

## 1.0.5 (2024-02-08)
* Add `List<Product>.getByReference(String)` helper extension method.
* Bump kotlinx-serialization-json to 1.6.2, browser to 1.7.8, core-ktx to 1.12.0, coroutines to 1.7.3
* Bump Kotlin to 1.9.22, Gradle plugin to 8.2.2
* Bump okhttp to 4.12.0, browser to 1.6.0

## 1.0.4 (2023-08-28)
* Bump browser to 1.6.0
* Bump androidBuildToolsVersion to 34.0.0, androidCompileSdkVersion and androidTargetSdkVersion to 34

## 1.0.3 (2023-08-02)
* `ProductRepository.catalogs` now represents catalogs mapped to a phone number instead of flat list of catalogs.
  Added support methods `ProductRepository.getCatalogs(phoneNumber)`, `ProductRepository.getAllCatalogs()` and `ProductRepository.clear()`
* Bump Gradle plugin to 8.1.0

## 1.0.2 (2023-06-05)
* Add `paymentVerified` property to `Subscription`
* Fix `OrderService.performRewardOrder` requires authentication
* Bump Gradle plugin to 8.0.2, Kotlin to 1.8.21, okhttp to 4.11.0, coroutines to 1.7.0, test framework dependencies

## 1.0.1 (2023-04-21)
* Fix `AgreementService.getAgreements()` does not require authentication.
* Add support to define payment and sim registration portals method for opening urls (embedded vs external browser) by adding 
  `urlLaunchMethod` property to `SimRegistrationConfig` and `RefillServiceConfig`
* Bump Kotlin to 1.8.20, Gradle plugin to 8.0.0, serialization-json to 1.5.0

## 1.0.0 (2023-02-20)
* Rename base classes: `SimRegistrationService` to `SimRegistrationModule`, `RefillService` to `RefillModule`,
  `ContentService` to `ContentModule` and `ProductService` to `ProductModule`
* All modules now have a `service` and a `repository`. All logic to access the repository data from a
  service has been refactored to be exposed via repository, while a services are responsible for backend
  communication and handling of results exclusively
* Module config classes have been renamed:
  `ContentServiceConfig` to `ContentConfig`, `ProductServiceConfig` to `ProductConfig`, `RefillServiceConfig` to `RefillConfig` and `SimRegistrationServiceConfig` to `SimRegistrationConfig`
* Module config classes have been moved to modules config package folders.
* Rename `AutoTopUp` to `Subscription` across all methods and related objects.
* Rename `TopUp` to `Order` across all methods and related objects.
* Any authenticated call to `RefillModule`´s services with invalid token will trigger and return `Result.Failure` before attempting to make a call.
* Shortcuts to refill modules have been removed from `RefillSdk`, which now references only its modules.
* Remove `CatalogList`, `GroupList` and `ProductList` objects in favour of direct static extensions
* Bump Kotlin to 1.7.21, Gradle plugin to 7.4.1, browser to 1.5.0, androidBuildToolsVersion to 33.0.2

## 0.11.8 (2023-01-13)
* Bump Kotlin to 1.7.20, Gradle plugin to 7.3.1, serialization-json to 1.4.1
* Bump androidCompileSdkVersion, androidTargetSdkVersion to 33 androidBuildToolsVersion to 33.0.1

## 0.11.7 (2022-09-26)
* Bump Kotlin to 1.7.10, Gradle plugin to 7.3.0, kotlinx-coroutines to 1.6.4, 
  serialization-json to 1.4.0, core-ktx to 1.9.0
* Bump androidBuildToolsVersion to 33.0.0, androidCompileSdkVersion to 33, androidTargetSdkVersion to 33
* Add `ContentProperties.rangeMin` and `ContentProperties.rangeMax` 

## 0.11.6 (2022-07-12)
* Enable StatusService for production environment

## 0.11.5 (2022-06-27)
* Enable sim registration for production environment
* Fix support for alternative date-time formats
* Fix how we handle customer identification internally
* Kotlin to 1.7.0, Gradle plugin to 7.2.1, Bump okhttp3 to 4.10.0, kotlinx-coroutines to 1.6.3

## 0.11.4 (2022-06-07)
* Add support for `LanguageConfig.NORWEGIAN`
* Fix potential `IndexOutOfBoundsException` when calling `HttpModule.initCalls`

## 0.11.3 (2022-05-23)
* Add support for newly added customers

## 0.11.2 (2022-05-19)
* Update `AgreementService` is using newer APIs
* Bump Gradle plugin to 7.2.0, kotlinx-serialization-json to 1.3.3

## 0.11.1 (2022-05-02)
* Add support for performing topUp with only bonus points in `TopUpService`
* Add `fetchTopUp` to `TopUpService`
* Fix `Phone.expirationDateAt` now includes time
* Fix backend dates are returned as UTC explicitly
* Fix `Bucket.expirationDateAt` now includes time
* Bump kotlin to 1.6.21, androidBuildToolsVersion to 32.0.0, androidCompileSdkVersion and
  androidTargetSdkVersion to 32

## 0.11.0 (2022-04-12)
* Update `coroutineScope` no longer required on calls using portal. Methods now use suspend modifier
  and return result directly instead of through callback, to match api behaviour across the SDK
  Affected methods: `TopUpService.performAnonymousTopUp`, `TopUpService.performTopUp`, 
  `AutoTopUpService.performAnonymousAutoTopUp`, `AutoTopUpService.performAutoTopUp` and 
  `SimRegistrationService.registerPhoneNumber`
* Add `Phone.balanceCurrency`, `Phone.mobileSubType`, `Phone.balanceAllowed`, `Phone.remainingVoiceCalls`, 
  `Phone.bonusBalanceAmount`, `Phone.remainingBonusRefills` and `Phone.requiredBonusRefills`
* Add `Product.ContentProperties.pointsAmount`, `Product.ContentProperties.pointsEarned`
  and `Product.ContentProperties.pointsAdjustedAmount`
* Fix `Phone.mobileType` is now exposed as `Phone.mobileTypeValue`,`Phone.nextExpiringPointsTime`as 
  `Phone.nextExpiringPointsTimeAt`
* Fix `Product.ContentProperties.originalAmount` is of type `Double`
* Fix to always check if current coroutine job is still active before invoking predicates or results
* Fix potential crash when opening embedded browser if the hosting device doesn't have it or is in
  any way defective. The flow will report a failed result instead.
* Rename `TopUpService.getAnonymousTopUps` to `TopUpService.getAnonymousTopUpStatuses`
* Bump kotlin to 1.6.20, coroutines to 1.6.1, Gradle to 7.1.3

## 0.10.1 (2022-03-11)
* Add property `Event.isRead`, and `RewardsService.setEventRead` method to locally persist and handle
  event read state.
* Add `createdAt` properties to `Event` and `Voucher` returning `Date` directly and change `created`
  modifier to internal
* Add `performRewardTopUp` to `TopUpService`
* Add `errorMessage` to all `GlobalErrorListener` callbacks
* Fix always retain existing `Phone.Bucket` info when updating phone
* Fix clear sensitive data from repository (eg. phones, topUps, rewards) when user is logged out.
* Fix formatting date when using non-latin locale

## 0.10.0 (2022-02-03)
* Add `SimRegistrationService`.
* Add `RewardsService` to Refill
* Add `validatePassword` to `AuthenticationService`
* Add optional property to define CoroutineScope when launching payment portal wit
  `AutoTopUpService.performAnonymousAutoTopUp`, `AutoTopUpService.performAutoTopUp`,
  `CardRegistrationService.registerCard`, `TopUpService.performAutoTopUp` and `TopUpService.performTopUp`
* Add optional property `includeTopUpsAndAutoTopUps` to `PhoneService.fetchPhone`, 
  `PhoneService.fetchPhonesForCustomer` and `PhoneService.addPhone`
* Fix potential issue where `onResumed()` may be called before `onNewIntent()` on some devices and
  thus wrongfully reporting that payment failed due to `ErrorType.CANCELLED_BY_USER`.
* Bump kotlin to 1.6.10, androidBuildToolsVersion to 31.0.0, okhttp to 4.9.3, Coroutines to 1.6.0,
  serialization-json to 1.3.2, Gradle to 7.1.0

## 0.8.4 (2021-12-08)
* Add `fetchPhoneNumberType` to `PhoneService`
* Fixed bug with adding auto top-up even if the payment failed
* Bump dependencies

## 0.8.3 (2021-11-09)
* Add `updatePassword` to `AuthenticationService`
* Bump androidx.browser:browser dependency to 1.4.0 and androidx.core:core-ktx to 1.7.0
* Bump compileSdkVersion and targetSdkVersion to 31

## 0.8.2 (2021-10-05)
* Add `ErrorType.CANCELLED_BY_USER` for cases where we know user cancellation is the reason resulting 
  in a `Result.Failure`
* Rename `cancelTopUpAndAnonymousTopUpFlows` to `cancelWaitingForSwishResult` 
* Fix behaviour when doing multiple topUps using Swish (due to redundant internal use of Coroutines)
* Override `equals` and `hashCode` in `Product` to compare only based on the `reference` property
* Bump Kotlin and Dokka to 1.5.31, 
* Bump kotlinx-coroutines-android to 1.5.2, kotlinx-serialization-json to 1.3.0

## 0.8.1 (2021-09-22)
* Add `onResumedAppLifecycle` into `RefillSdk` allowing services to react and modify behaviour based 
  on app changing to foreground (e.ex. allows Payment Portal to detect cancelled flow)
* Update `Media` object from `ContentService` is now retrieved by name instead of id by default
* Remove `PaymentMethod` option `AutoTopUpService` as only paying with card is supported for auto top-ups
* Rename `getUrl` to `getMediaUrl` in `ContentService`
* Fix failing orders using Swish

## 0.8.0 (2021-09-20)
* Add `ReceiptService`
* Fix deserialization only happens once when reading object or list from repository
* Verify `RefillServiceConfig.intentFilterScheme` on init and throw IllegalStateException if it contains
  special characters or numerics as payment portal does not support those

## 0.7.0 (2021-09-13)
* Add support for fetching `TopUps` and `AutoTopUps`.
* Add support for query based on tags when fetching texts or medias in `ContentService`
* Update `RefillRepository` to update the phones' TopUps and AutoTopUps
* Update dates are always nullable if backend date strings are nullable
* Update all `Date` instances represented by backend date strings are now val instead of fun  
* Fix `Phone.phones` order will now be retained when updated
* Fix `RefillRepository.addOrUpdateTopUpStatus` updating `anonymousTopUpStatuses` instead of `topUpStatuses`
* Fix predicate to handle calls that don't have a response body
* Fix name issues in `Bucket`
* Add DateTime format pattern to parse the time part in backend date strings

## 0.6.1 (2021-08-19)
* Fix incorrect refill url for production environment
* Fix property names in `Bucket` for calls, kb, minutes, sms and mms count

## 0.6.0 (2021-08-18)
* Add `sources.jar` and `javadoc.jar` to build package
* Add support for authenticated auto top-ups to `AutoTopUpService`
* Add support for authenticated top-ups to `TopUpService`
* Add `AgreementService`
* Add `isLoggedIn()` convenience method to `AuthenticationService`
* Add `CardRegistrationService`, a sub-service of `RefillService`
* Add `PhoneService`, a sub-service of `RefillService`
* Add `cachingMethod` to `ContentConfig`, allowing us to define behaviour of persisted content data 
  to avoid redundant calls to backend
* Rename internal shared preferences files in `ContentService` and `RefillService` for consistency. 
  Will cause any currently logged in users to be logged out.
  
## 0.5.0 (2021-07-26)
* Add `AuthenticationService` and `CustomerService`
* Add optional `resultsListener` to `initCalls` method in addition to existing aggregated 
  result `resultListener` callback
* Add `VERSION_NAME` and `BUILD_TIMESTAMP` to library `BuildConfig`
* Add `x-smartrefill-api-version` header representing `BuildConfig.VERSION_NAME`
* Rename `getAnonymousAutoTopUp()` to `getAnonymousAutoTopUps()`
* Rename `AutoTopUp` to `AutoTopUpStatus` to keep consistency across services
* Rename `getTopUpStatuses()` to `getAnonymousTopUps()`
* Fix `resultListener` in `initCall` and `initCalls` are now optional
* Fix documentation and log messages to reflect renaming

## 0.4.0 (2021-06-28)
* Add `article` property to `TopUpStatus`
* Remove properties `status`, `selectPaymentMethodURL`, `subscriptionAllowed`, `receiptEnabled`,
  `earnedPoints` from `TopUpStatus
* Fix failed order when trying to fetch info in latest step
* Rename `OrderService` to `TopUpService`
* Rename `OrderRequest`, `OrderStatus` to `TopUpRequest,` `TopUpStatus`
* Rename `SubscriptionService` to `AutoTopUpService`
* Rename `Subscription` to `AutoTopUp`, `SubscriptionRequest` tp `AutoTopUpRequest`
* Rename `PmsProduct` to `Product`, `PmsCatalog` to `Catalog`, `PmsGroup` to `Group`
* Bump Kotlin to 1.5.20

## 0.3.0 (2021-06-17)
* Add `order()` extension to list of `Group`, `Catalog` and `Product`
* Add objects `CatalogList`, `GroupList` and `ProductList` acting as wrappers for product extension 
  functions, making documentation look cleader
* Add `getCatalogById()` extension to `CatalogList`
* Add `SubscriptionService` with support for anonymous subscriptions
* Remove `SubscriptionInfo` and `TopUpInfo` in favour of public rest objects (`TopUpRequest`,
  `SubscriptionRequest`) when initializing anonymous topup and subscription
* Fix `maven-metadata.xml` missing in package registry

## 0.2.0 (2021-06-14)
* First public release
